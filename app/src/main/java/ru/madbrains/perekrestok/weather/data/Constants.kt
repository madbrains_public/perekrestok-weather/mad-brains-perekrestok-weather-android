package ru.madbrains.perekrestok.weather.data

object Constants {

    const val TELEPORT_BASE_URL = "https://api.teleport.org/api/"
    const val OPEN_WEATHER_BASE_URL = "https://api.openweathermap.org/"
    const val API_KEY = "a9d3c90d64871c2a2c54df6399e6f48d"
    const val API_LANG = "ru"
    const val API_UNITS = "metric"
    const val MOSCOW_ID = "524901"
    const val MOSCOW = "Moscow"
    const val MINSK_ID = "625144"
    const val MINSK = "Minsk"
    const val GEONAME_ID = "https://api.teleport.org/api/cities/geonameid:"
}