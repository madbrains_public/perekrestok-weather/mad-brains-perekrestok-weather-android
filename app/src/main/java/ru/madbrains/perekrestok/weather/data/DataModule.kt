package ru.madbrains.perekrestok.weather.data

import androidx.room.Room
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.madbrains.perekrestok.weather.data.Constants.OPEN_WEATHER_BASE_URL
import ru.madbrains.perekrestok.weather.data.Constants.TELEPORT_BASE_URL
import ru.madbrains.perekrestok.weather.data.api.OpenWeatherApi
import ru.madbrains.perekrestok.weather.data.api.TeleportApi
import ru.madbrains.perekrestok.weather.data.db.CitiesDatabase
import ru.madbrains.perekrestok.weather.data.repositories.WeatherRepositoryImpl
import ru.madbrains.perekrestok.weather.domain.interfaces.WeatherRepository
import timber.log.Timber
import java.util.concurrent.TimeUnit

object DataModule {

    fun create() = module {
        single {
            createOpenWeatherApi(
                createHttpClient(),
                get()
            )
        }

        single {
            createTeleportApi(
                createHttpClient(),
                get()
            )
        }

        single { createMoshi() }

        single {
            Room
                .databaseBuilder(
                    get(),
                    CitiesDatabase::class.java,
                    "cities_database"
                )
                .build()
        }

        single { get<CitiesDatabase>().citiesDao() }

        factory { WeatherRepositoryImpl(get(), get(), get()) as WeatherRepository }
    }

    private fun createOpenWeatherApi(client: OkHttpClient, moshi: Moshi): OpenWeatherApi {
        return Retrofit.Builder()
            .baseUrl(OPEN_WEATHER_BASE_URL)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
            .create(OpenWeatherApi::class.java)
    }

    private fun createTeleportApi(client: OkHttpClient, moshi: Moshi): TeleportApi {
        return Retrofit.Builder()
            .baseUrl(TELEPORT_BASE_URL)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
            .create(TeleportApi::class.java)
    }

    private fun createHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        with(builder) {

            connectTimeout(30, TimeUnit.SECONDS)
            readTimeout(30, TimeUnit.SECONDS)
            writeTimeout(30, TimeUnit.SECONDS)

            addNetworkInterceptor(loggingInterceptor())
        }
        return builder.build()
    }

    private fun loggingInterceptor(): Interceptor {
        val logger = object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Timber.tag("OkHttp").d(message)
            }
        }
        return HttpLoggingInterceptor(logger).apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    private fun createMoshi(): Moshi {
        return Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }
}