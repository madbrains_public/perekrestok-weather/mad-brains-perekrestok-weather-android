package ru.madbrains.perekrestok.weather.data.api

import retrofit2.http.GET
import retrofit2.http.Query
import ru.madbrains.perekrestok.weather.data.api.model.CitiesListApiModel
import ru.madbrains.perekrestok.weather.domain.model.CityWeather
import ru.madbrains.perekrestok.weather.domain.model.DetailedWeather

interface OpenWeatherApi {

    @GET("data/2.5/group")
    suspend fun getWeatherForCities(
        @Query("id", encoded = true) id: String,
        @Query("units") units: String,
        @Query("appid") apiKey: String,
        @Query("lang") language: String
    ): CitiesListApiModel

    @GET("data/2.5/weather")
    suspend fun getWeatherForOneCity(
        @Query("id") id: String,
        @Query("units") units: String,
        @Query("appid") apiKey: String,
        @Query("lang") language: String
    ): CityWeather

    @GET("data/2.5/onecall")
    suspend fun getDetailedWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("units") units: String,
        @Query("appid") apiKey: String,
        @Query("lang") language: String
    ): DetailedWeather
}