package ru.madbrains.perekrestok.weather.data.api

import retrofit2.http.GET
import retrofit2.http.Query
import ru.madbrains.perekrestok.weather.data.api.model.SearchResultApiModel

interface TeleportApi {

    @GET("cities/")
    suspend fun searchCity(
        @Query("search") query: String
    ): SearchResultApiModel
}