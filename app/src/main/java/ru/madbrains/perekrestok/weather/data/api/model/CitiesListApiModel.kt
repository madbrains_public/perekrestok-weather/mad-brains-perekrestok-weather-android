package ru.madbrains.perekrestok.weather.data.api.model

import ru.madbrains.perekrestok.weather.domain.model.CityWeather

data class CitiesListApiModel(
    val list: List<CityWeather>
)