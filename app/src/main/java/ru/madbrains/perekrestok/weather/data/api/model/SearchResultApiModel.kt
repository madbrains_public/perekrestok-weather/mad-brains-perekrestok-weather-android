package ru.madbrains.perekrestok.weather.data.api.model

import com.squareup.moshi.Json
import ru.madbrains.perekrestok.weather.data.Constants

data class SearchResultApiModel(
    @Json(name = "_embedded") val embedded: Result
) {
    data class Result(
        @Json(name = "city:search-results") val result: List<CitySearchResultApiModel>
    ) {
        data class CitySearchResultApiModel(
            @Json(name = "_links") val link: Link,
            @Json(name = "matching_alternate_names") val names: List<CityName>,
            @Json(name = "matching_full_name") val fullName: String
        ) {
            data class Link(
                @Json(name = "city:item")val item: CityItem
            ) {
                data class CityItem(
                    @Json(name = "href") val _id: String
                ) {
                    val id: String
                        get() = _id.removePrefix(Constants.GEONAME_ID).removeSuffix("/")
                }
            }
            data class CityName(
                val name: String
            )
        }
    }
}