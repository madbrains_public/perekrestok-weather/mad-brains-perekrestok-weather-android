package ru.madbrains.perekrestok.weather.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.madbrains.perekrestok.weather.data.db.dao.CitiesDao
import ru.madbrains.perekrestok.weather.data.db.model.CityDBModel

@Database(entities = [CityDBModel::class], version = 1)
abstract class CitiesDatabase : RoomDatabase() {

    abstract fun citiesDao(): CitiesDao
}