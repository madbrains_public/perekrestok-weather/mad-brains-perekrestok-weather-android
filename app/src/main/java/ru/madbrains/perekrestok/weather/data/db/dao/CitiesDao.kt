package ru.madbrains.perekrestok.weather.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.madbrains.perekrestok.weather.data.db.model.CityDBModel

@Dao
interface CitiesDao {

    @Query("SELECT * FROM cities")
    suspend fun getAll(): List<CityDBModel>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun add(repository: CityDBModel)
}