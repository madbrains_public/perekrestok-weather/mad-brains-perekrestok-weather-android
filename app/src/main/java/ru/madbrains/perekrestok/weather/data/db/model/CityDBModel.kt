package ru.madbrains.perekrestok.weather.data.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cities")
data class CityDBModel(
    @PrimaryKey val id: String,
    val name: String
)