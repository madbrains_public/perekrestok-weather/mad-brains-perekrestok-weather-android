package ru.madbrains.perekrestok.weather.data.repositories

import ru.madbrains.perekrestok.weather.data.Constants.API_KEY
import ru.madbrains.perekrestok.weather.data.Constants.API_LANG
import ru.madbrains.perekrestok.weather.data.Constants.API_UNITS
import ru.madbrains.perekrestok.weather.data.Constants.MINSK
import ru.madbrains.perekrestok.weather.data.Constants.MINSK_ID
import ru.madbrains.perekrestok.weather.data.Constants.MOSCOW
import ru.madbrains.perekrestok.weather.data.Constants.MOSCOW_ID
import ru.madbrains.perekrestok.weather.data.api.OpenWeatherApi
import ru.madbrains.perekrestok.weather.data.api.TeleportApi
import ru.madbrains.perekrestok.weather.domain.interfaces.WeatherRepository
import ru.madbrains.perekrestok.weather.domain.model.CityWeather
import ru.madbrains.perekrestok.weather.data.db.dao.CitiesDao
import ru.madbrains.perekrestok.weather.data.db.model.CityDBModel
import ru.madbrains.perekrestok.weather.domain.model.CitySearchResult
import ru.madbrains.perekrestok.weather.domain.model.DetailedWeather
import java.lang.StringBuilder

class WeatherRepositoryImpl(
    private val openWeatherApi: OpenWeatherApi,
    private val teleportApi: TeleportApi,
    private val dao: CitiesDao
) : WeatherRepository {
    override suspend fun getWeatherList(): List<CityWeather> {

        var cities = dao.getAll()
        val cityWeatherList = mutableListOf<CityWeather>()
        if (cities.isEmpty()) {
            prepopulateDB()
            cities = dao.getAll()
        }
        val idsString = StringBuilder()
        for (city in cities.withIndex()) {
            idsString.append(city.value.id)
                    .append(",")
            if (city.index % 15 == 0 || city.index != 0) {
                cityWeatherList.addAll(openWeatherApi.getWeatherForCities(idsString.toString(), API_UNITS, API_KEY, API_LANG).list)
                idsString.clear()
            } else if (city.index == cities.lastIndex) {
                cityWeatherList.addAll(openWeatherApi.getWeatherForCities(idsString.toString(), API_UNITS, API_KEY, API_LANG).list)
                idsString.clear()
            }
        }
        return cityWeatherList
    }

    override suspend fun getDetailedWeather(lat: Double, lon: Double): DetailedWeather {
        return openWeatherApi.getDetailedWeather(lat, lon, API_UNITS, API_KEY, API_LANG)
    }

    override suspend fun searchCity(query: String): List<CitySearchResult> {
        val result = mutableListOf<CitySearchResult>()
        for (city in teleportApi.searchCity(query).embedded.result) {
            if (city.names.isNotEmpty()) {
                result.add(CitySearchResult(city.link.item.id, city.names.first().name, city.fullName))
            }
        }
        return result
    }

    override suspend fun addCityToDB(id: String) {
        val cityWeather = openWeatherApi.getWeatherForOneCity(id, API_UNITS, API_KEY, API_LANG)
        dao.add(CityDBModel(cityWeather.id.toString(), cityWeather.name))
    }

    private suspend fun prepopulateDB() {
        dao.add(CityDBModel(MOSCOW_ID, MOSCOW))
        dao.add(CityDBModel(MINSK_ID, MINSK))
    }
}