package ru.madbrains.perekrestok.weather.domain

import org.koin.dsl.module
import ru.madbrains.perekrestok.weather.domain.interactors.WeatherInteractor

object DomainModule {

    fun create() = module {
        factory { WeatherInteractor(get()) }
    }
}