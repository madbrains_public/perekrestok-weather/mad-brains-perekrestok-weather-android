package ru.madbrains.perekrestok.weather.domain.interactors

import ru.madbrains.perekrestok.weather.domain.interfaces.WeatherRepository
import ru.madbrains.perekrestok.weather.domain.model.CitySearchResult
import ru.madbrains.perekrestok.weather.domain.model.CityWeather
import ru.madbrains.perekrestok.weather.domain.model.DetailedWeather

class WeatherInteractor(
    private val repository: WeatherRepository
) {

    suspend fun getWeatherList(): List<CityWeather> {
        return repository.getWeatherList()
    }

    suspend fun getDetailedWeather(lat: Double, lon: Double): DetailedWeather {
        return repository.getDetailedWeather(lat, lon)
    }

    suspend fun searchCity(query: String): List<CitySearchResult> {
        return repository.searchCity(query)
    }

    suspend fun addCityToDB(id: String) {
        repository.addCityToDB(id)
    }
}