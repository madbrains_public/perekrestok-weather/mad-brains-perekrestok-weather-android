package ru.madbrains.perekrestok.weather.domain.interfaces

import ru.madbrains.perekrestok.weather.domain.model.CitySearchResult
import ru.madbrains.perekrestok.weather.domain.model.CityWeather
import ru.madbrains.perekrestok.weather.domain.model.DetailedWeather

interface WeatherRepository {

    suspend fun getWeatherList(): List<CityWeather>

    suspend fun getDetailedWeather(lat: Double, lon: Double): DetailedWeather

    suspend fun searchCity(query: String): List<CitySearchResult>

    suspend fun addCityToDB(id: String)
}