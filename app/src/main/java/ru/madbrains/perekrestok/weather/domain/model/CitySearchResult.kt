package ru.madbrains.perekrestok.weather.domain.model

data class CitySearchResult(
    val id: String,
    val name: String,
    val fullName: String
)