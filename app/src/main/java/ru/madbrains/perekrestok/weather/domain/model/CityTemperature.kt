package ru.madbrains.perekrestok.weather.domain.model

import com.squareup.moshi.Json

data class CityTemperature(
    @Json(name = "temp") val now: Double,
    @Json(name = "feels_like") val feelsLike: Double,
    @Json(name = "temp_min") val min: Double,
    @Json(name = "temp_max") val max: Double
)