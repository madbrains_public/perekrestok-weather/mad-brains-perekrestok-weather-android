package ru.madbrains.perekrestok.weather.domain.model

import com.squareup.moshi.Json

data class CityWeather(
    val id: Long,
    val name: String,
    @Json(name = "main") val cityTemperature: CityTemperature,
    val weather: List<Weather>,
    @Json(name = "coord") val coordinates: Coordinates
) {
    data class Coordinates(
        val lon: Double,
        val lat: Double
    )
}