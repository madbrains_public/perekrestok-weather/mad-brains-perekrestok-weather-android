package ru.madbrains.perekrestok.weather.domain.model

data class DetailedWeather(
    val current: TemperatureToday,
    val hourly: List<TemperatureToday>,
    val daily: List<TemperatureForecast>,
    val timezone: String
)