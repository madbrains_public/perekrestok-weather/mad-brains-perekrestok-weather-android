package ru.madbrains.perekrestok.weather.domain.model

import com.squareup.moshi.Json

data class TemperatureForecast(
    @Json(name = "temp") val tempDayNight: TempDayNight,
    val weather: List<Weather>,
    @Json(name = "dt") val date: Long
) {
    data class TempDayNight(
        val day: Double,
        val night: Double
    )
}