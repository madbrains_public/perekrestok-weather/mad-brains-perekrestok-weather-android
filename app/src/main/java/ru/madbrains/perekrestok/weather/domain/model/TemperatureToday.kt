package ru.madbrains.perekrestok.weather.domain.model

import com.squareup.moshi.Json

data class TemperatureToday(
    @Json(name = "temp") val now: Double,
    @Json(name = "feels_like") val feelsLike: Double,
    @Json(name = "dt") val date: Long,
    val weather: List<Weather>,
    val sunrise: Long?,
    val sunset: Long?,
    val pressure: Int,
    val humidity: Int,
    val visibility: Int,
    @Json(name = "wind_speed") val windSpeed: Double
)