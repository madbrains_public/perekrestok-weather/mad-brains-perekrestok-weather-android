package ru.madbrains.perekrestok.weather.domain.model

data class Weather(
    val description: String,
    val icon: String
)