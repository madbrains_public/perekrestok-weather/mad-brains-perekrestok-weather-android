package ru.madbrains.perekrestok.weather.presentation

import android.content.Context
import androidx.fragment.app.Fragment
import ru.madbrains.perekrestok.weather.R
import ru.madbrains.perekrestok.weather.presentation.ui.BaseActivity
import kotlin.math.floor
import kotlin.math.roundToInt

fun Fragment.changeProgressVisibility(visibility: Boolean) {
    (requireActivity() as? BaseActivity)?.changeProgressVisibility(visibility)
}

fun Double.toCelsiusString(context: Context): String {
    return if (this < 1) {
        context.getString(R.string.celsius, floor(this).toInt())
    } else {
        context.getString(R.string.celsius_plus, this.roundToInt())
    }
}
