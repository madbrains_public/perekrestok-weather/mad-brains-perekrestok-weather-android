package ru.madbrains.perekrestok.weather.presentation.di

import org.koin.core.module.Module
import ru.madbrains.perekrestok.weather.data.DataModule
import ru.madbrains.perekrestok.weather.domain.DomainModule

object Modules {

    fun allModules(): List<Module> {
        return listOf(
            DataModule.create(),
            PresentationModule.create(),
            DomainModule.create()
        )
    }
}