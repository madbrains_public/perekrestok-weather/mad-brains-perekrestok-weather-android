package ru.madbrains.perekrestok.weather.presentation.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.madbrains.perekrestok.weather.presentation.ui.detail.DetailViewModel
import ru.madbrains.perekrestok.weather.presentation.ui.mainList.MainListViewModel
import ru.madbrains.perekrestok.weather.presentation.ui.search.SearchViewModel

object PresentationModule {

    fun create() = module {
        viewModel { MainListViewModel(get()) }
        viewModel { DetailViewModel(get()) }
        viewModel { SearchViewModel(get()) }
    }
}