package ru.madbrains.perekrestok.weather.presentation.ui

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.loader.*

abstract class BaseActivity : AppCompatActivity() {

    private var allowBack: Boolean = true

    fun changeProgressVisibility(visibility: Boolean) {
        if (visibility) {
            loader?.visibility = View.VISIBLE
            allowBack = false
        } else {
            loader?.visibility = View.GONE
            allowBack = true
        }
    }

    override fun onBackPressed() {
        if (!allowBack) return
        super.onBackPressed()
    }
}