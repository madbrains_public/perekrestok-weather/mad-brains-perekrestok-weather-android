package ru.madbrains.perekrestok.weather.presentation.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {

    val progressVisibility = MutableLiveData<Boolean>()
}