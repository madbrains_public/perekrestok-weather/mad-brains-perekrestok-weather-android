package ru.madbrains.perekrestok.weather.presentation.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.madbrains.perekrestok.weather.R
import ru.madbrains.perekrestok.weather.domain.model.TemperatureToday
import ru.madbrains.perekrestok.weather.presentation.toCelsiusString
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.Date
import java.util.TimeZone

class DayForecastAdapter : ListAdapter<TemperatureToday, DayForecastViewHolder>(TemperatureTodayDiff) {

    private lateinit var timeZone: String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayForecastViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return DayForecastViewHolder(
            inflater.inflate(R.layout.forecast_day_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: DayForecastViewHolder, position: Int) {

        val forecast = getItem(position)
        forecast?.let {
            holder.bind(it, timeZone)
        }
    }

    fun setTimeZone(timeZone: String) {
        this.timeZone = timeZone
    }
}

class DayForecastViewHolder(
    view: View
) : RecyclerView.ViewHolder(view) {

    private val hour: TextView = itemView.findViewById(R.id.hour)
    private val icon: ImageView = itemView.findViewById(R.id.icon)
    private val temperature: TextView = itemView.findViewById(R.id.temperature)

    fun bind(temperatureToday: TemperatureToday, timeZone: String?) {
        val dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        timeZone?.let {
            dateFormat.timeZone = TimeZone.getTimeZone(it)
        }
        hour.text = dateFormat.format(Date(temperatureToday.date * 1000L))
        Glide.with(itemView.context).load(itemView.context.getString(R.string.icon_link, temperatureToday.weather.first().icon)).into(icon)
        temperature.text = temperatureToday.now.toCelsiusString(itemView.context)
    }
}

object TemperatureTodayDiff : DiffUtil.ItemCallback<TemperatureToday>() {
    override fun areItemsTheSame(
        oldItem: TemperatureToday,
        newItem: TemperatureToday
    ): Boolean {
        return oldItem.date == newItem.date
    }

    override fun areContentsTheSame(oldItem: TemperatureToday, newItem: TemperatureToday): Boolean {
        return oldItem == newItem
    }
}