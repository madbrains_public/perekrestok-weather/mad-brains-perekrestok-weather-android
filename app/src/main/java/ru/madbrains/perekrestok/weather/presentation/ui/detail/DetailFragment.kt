package ru.madbrains.perekrestok.weather.presentation.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.detail_fragment.*
import kotlinx.android.synthetic.main.detail_fragment.refreshLayout
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.madbrains.perekrestok.weather.R
import ru.madbrains.perekrestok.weather.domain.model.TemperatureToday
import ru.madbrains.perekrestok.weather.presentation.EventObserver
import ru.madbrains.perekrestok.weather.presentation.changeProgressVisibility
import ru.madbrains.perekrestok.weather.presentation.toCelsiusString
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.TimeZone
import java.util.Date

class DetailFragment : Fragment() {

    private val viewModel by viewModel<DetailViewModel>()
    private lateinit var weekAdapter: WeekForecastAdapter
    private lateinit var dayAdapter: DayForecastAdapter
    companion object {
        const val CITY_NAME = "city_name"
        const val CITY_LAT = "city_lat"
        const val CITY_LON = "city_lon"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arguments?.let { it ->
            weekAdapter = WeekForecastAdapter()
            weekForecastList.adapter = weekAdapter
            weekForecastList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            dayAdapter = DayForecastAdapter()
            dayForecastList.adapter = dayAdapter
            dayForecastList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            viewModel.getForecast(it.getDouble(CITY_LAT), it.getDouble(CITY_LON))
            viewModel.detailedWeather.observe(
                viewLifecycleOwner,
                {
                    dayAdapter.setTimeZone(it.timezone)
                    dayAdapter.submitList(it.hourly.subList(0, 24))
                    weekAdapter.submitList(it.daily.subList(0, 7))
                    setUpInfo(arguments?.getString(CITY_NAME) ?: "", it.current, it.timezone)
                }
            )
            refreshLayout.setOnRefreshListener {
                refreshLayout.isRefreshing = false
                viewModel.getForecast(it.getDouble(CITY_LAT), it.getDouble(CITY_LON))
            }

            viewModel.showProblemMessage.observe(
                    viewLifecycleOwner,
                    EventObserver {
                        Snackbar.make(requireView(), R.string.error_text, Snackbar.LENGTH_SHORT).show()
                    }
            )

            viewModel.progressVisibility.observe(viewLifecycleOwner, { visibility ->
                changeProgressVisibility(visibility)
            })
        }
    }

    private fun setUpInfo(name: String, temperatureToday: TemperatureToday, timeZone: String) {
        cityName.text = name
        description.text = temperatureToday.weather.first().description.capitalize(Locale.getDefault())
        cardView.visibility = View.VISIBLE
        Glide.with(requireContext()).load(getString(R.string.icon_link_hi_res, temperatureToday.weather.first().icon)).into(icon)
        temperature.text = temperatureToday.now.toCelsiusString(requireContext())
        feelsLike.text = getString(R.string.feelsLike, temperatureToday.feelsLike.toCelsiusString(requireContext()))
        windSpeed.text = getString(R.string.wind_speed, temperatureToday.windSpeed)
        humidity.text = getString(R.string.humidity, temperatureToday.humidity)
        pressure.text = getString(R.string.pressure, temperatureToday.pressure)
        visibility.text = getString(R.string.visibility, temperatureToday.visibility)

        val dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone(timeZone)
        sunrise.text = getString(R.string.sunrise, dateFormat.format(Date((temperatureToday.sunrise ?: 0) * 1000L)))
        sunset.text = getString(R.string.sunset, dateFormat.format(Date((temperatureToday.sunset ?: 0) * 1000L)))
    }
}
