package ru.madbrains.perekrestok.weather.presentation.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import ru.madbrains.perekrestok.weather.domain.interactors.WeatherInteractor
import ru.madbrains.perekrestok.weather.domain.model.DetailedWeather
import ru.madbrains.perekrestok.weather.presentation.Event
import ru.madbrains.perekrestok.weather.presentation.ui.BaseViewModel
import timber.log.Timber

class DetailViewModel(
    private val interactor: WeatherInteractor
) : BaseViewModel() {

    val showProblemMessage = MutableLiveData<Event<Unit>>()
    val detailedWeather = MutableLiveData<DetailedWeather>()

    private val errorHandler = CoroutineExceptionHandler { _, exception ->
        progressVisibility.value = false
        showProblemMessage.value = Event(Unit)
        Timber.w(exception)
    }

    fun getForecast(lat: Double, lon: Double) {
        viewModelScope.launch(errorHandler) {
            progressVisibility.value = true
            detailedWeather.value = interactor.getDetailedWeather(lat, lon)
            progressVisibility.value = false
        }
    }
}
