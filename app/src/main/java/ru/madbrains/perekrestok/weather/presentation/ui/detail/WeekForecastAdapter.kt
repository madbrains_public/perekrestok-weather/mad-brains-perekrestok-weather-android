package ru.madbrains.perekrestok.weather.presentation.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.madbrains.perekrestok.weather.R
import ru.madbrains.perekrestok.weather.domain.model.TemperatureForecast
import ru.madbrains.perekrestok.weather.presentation.toCelsiusString
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class WeekForecastAdapter : ListAdapter<TemperatureForecast, WeekForecastViewHolder>(TemperatureForecastDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeekForecastViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return WeekForecastViewHolder(
            inflater.inflate(R.layout.forecast_week_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: WeekForecastViewHolder, position: Int) {
        val temperatureForecast = getItem(position)
        temperatureForecast?.let {
            holder.bind(it)
        }
    }
}

class WeekForecastViewHolder(
    view: View
) : RecyclerView.ViewHolder(view) {

    private val day: TextView = itemView.findViewById(R.id.day)
    private val icon: ImageView = itemView.findViewById(R.id.icon)
    private val temperatureDay: TextView = itemView.findViewById(R.id.temperatureDay)
    private val temperatureNight: TextView = itemView.findViewById(R.id.temperatureNight)

    fun bind(temperatureForecast: TemperatureForecast) {
        day.text = SimpleDateFormat("EEEE", Locale.getDefault()).format(Date(temperatureForecast.date * 1000L))
        Glide.with(itemView.context).load(itemView.context.getString(R.string.icon_link, temperatureForecast.weather.first().icon)).into(icon)
        temperatureDay.text = temperatureForecast.tempDayNight.day.toCelsiusString(itemView.context)
        temperatureNight.text = temperatureForecast.tempDayNight.night.toCelsiusString(itemView.context)
    }
}

object TemperatureForecastDiff : DiffUtil.ItemCallback<TemperatureForecast>() {
    override fun areItemsTheSame(
        oldItem: TemperatureForecast,
        newItem: TemperatureForecast
    ): Boolean {
        return oldItem.date == newItem.date
    }

    override fun areContentsTheSame(oldItem: TemperatureForecast, newItem: TemperatureForecast): Boolean {
        return oldItem == newItem
    }
}