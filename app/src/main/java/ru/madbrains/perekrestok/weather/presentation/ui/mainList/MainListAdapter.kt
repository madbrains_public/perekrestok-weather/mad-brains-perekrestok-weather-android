package ru.madbrains.perekrestok.weather.presentation.ui.mainList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ru.madbrains.perekrestok.weather.R
import ru.madbrains.perekrestok.weather.domain.model.CityWeather
import ru.madbrains.perekrestok.weather.presentation.toCelsiusString

class MainListAdapter(
    private val listener: (CityWeather) -> Unit
) : ListAdapter<CityWeather, CityViewHolder>(CityDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CityViewHolder(
            inflater.inflate(R.layout.weather_item, parent, false)
        ) { position ->
            listener.invoke(getItem(position)!!)
        }
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        val cityWeather = getItem(position)
        cityWeather?.let {
            holder.bind(it)
        }
    }
}

class CityViewHolder(
    view: View,
    private val listener: (Int) -> Unit
) : RecyclerView.ViewHolder(view) {

    private val name: TextView = itemView.findViewById(R.id.cityName)
    private val icon: ImageView = itemView.findViewById(R.id.icon)
    private val temperature: TextView = itemView.findViewById(R.id.temperature)

    init {
        itemView.setOnClickListener { listener.invoke(adapterPosition) }
    }

    fun bind(cityWeather: CityWeather) {
        name.text = cityWeather.name
        Glide.with(itemView.context).load(itemView.context.getString(R.string.icon_link, cityWeather.weather.first().icon)).into(icon)
        temperature.text = cityWeather.cityTemperature.now.toCelsiusString(itemView.context)
    }
}

object CityDiff : DiffUtil.ItemCallback<CityWeather>() {
    override fun areItemsTheSame(
        oldItem: CityWeather,
        newItem: CityWeather
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CityWeather, newItem: CityWeather): Boolean {
        return oldItem == newItem
    }
}