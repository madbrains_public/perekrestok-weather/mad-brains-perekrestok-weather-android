package ru.madbrains.perekrestok.weather.presentation.ui.mainList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.main_list_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.madbrains.perekrestok.weather.R
import ru.madbrains.perekrestok.weather.domain.model.CityWeather
import ru.madbrains.perekrestok.weather.presentation.Event
import ru.madbrains.perekrestok.weather.presentation.EventObserver
import ru.madbrains.perekrestok.weather.presentation.changeProgressVisibility
import ru.madbrains.perekrestok.weather.presentation.ui.detail.DetailFragment

class MainListFragment : Fragment() {

    private val viewModel by viewModel<MainListViewModel>()
    private lateinit var adapter: MainListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = MainListAdapter { viewModel.navigateToDetails.value = Event(it) }
        cityWeatherList.adapter = adapter
        viewModel.cities.observe(
                viewLifecycleOwner,
                {
                    adapter.submitList(it)
                }
        )

        viewModel.navigateToDetails.observe(
                viewLifecycleOwner,
                EventObserver {
                    openDetails(it)
                }
        )

        viewModel.showProblemMessage.observe(
                viewLifecycleOwner,
                EventObserver {
                    Snackbar.make(requireView(), R.string.error_text, Snackbar.LENGTH_SHORT).show()
                }
        )

        viewModel.progressVisibility.observe(viewLifecycleOwner, { visibility ->
            changeProgressVisibility(visibility)
        })

        refreshLayout.setOnRefreshListener {
            refreshLayout.isRefreshing = false
            viewModel.getWeatherForCities()
        }
        fab.setOnClickListener {
            navigateToSearch()
        }
    }

    override fun onResume() {
        viewModel.getWeatherForCities()
        super.onResume()
    }

    private fun openDetails(cityWeather: CityWeather) {
        findNavController().navigate(
            R.id.action_mainListFragment_to_detailFragment,
            bundleOf(DetailFragment.CITY_NAME to cityWeather.name, DetailFragment.CITY_LAT to cityWeather.coordinates.lat, DetailFragment.CITY_LON to cityWeather.coordinates.lon)

        )
    }

    private fun navigateToSearch() {
        findNavController().navigate(
            R.id.action_mainListFragment_to_search_fragment
        )
    }
}
