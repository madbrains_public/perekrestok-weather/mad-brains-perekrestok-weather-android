package ru.madbrains.perekrestok.weather.presentation.ui.mainList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import ru.madbrains.perekrestok.weather.domain.interactors.WeatherInteractor
import ru.madbrains.perekrestok.weather.domain.model.CityWeather
import ru.madbrains.perekrestok.weather.presentation.Event
import ru.madbrains.perekrestok.weather.presentation.ui.BaseViewModel
import timber.log.Timber

class MainListViewModel(
    private val interactor: WeatherInteractor
) : BaseViewModel() {

    val navigateToDetails = MutableLiveData<Event<CityWeather>>()
    val showProblemMessage = MutableLiveData<Event<Unit>>()
    val cities = MutableLiveData<List<CityWeather>>()

    private val errorHandler = CoroutineExceptionHandler { _, exception ->
        progressVisibility.value = false
        showProblemMessage.value = Event(Unit)
        Timber.w(exception)
    }

    fun getWeatherForCities() {
        viewModelScope.launch(errorHandler) {
            progressVisibility.value = true
            cities.value = interactor.getWeatherList()
            progressVisibility.value = false
        }
    }
}
