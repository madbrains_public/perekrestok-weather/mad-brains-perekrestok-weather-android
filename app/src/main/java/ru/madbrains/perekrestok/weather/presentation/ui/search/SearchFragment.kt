package ru.madbrains.perekrestok.weather.presentation.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.search_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.madbrains.perekrestok.weather.R
import ru.madbrains.perekrestok.weather.presentation.Event
import ru.madbrains.perekrestok.weather.presentation.EventObserver
import ru.madbrains.perekrestok.weather.presentation.changeProgressVisibility

class SearchFragment : Fragment() {

    private val viewModel by viewModel<SearchViewModel>()
    private lateinit var adapter: SearchResultAdapter
    private lateinit var searchView: SearchView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = SearchResultAdapter { viewModel.chooseCity.value = Event(it) }
        searchResultList.adapter = adapter

        viewModel.searchResults.observe(
                viewLifecycleOwner,
                {
                    adapter.submitList(it)
                    if (it.isEmpty()) {
                        emptyResult.visibility = View.VISIBLE
                    } else {
                        emptyResult.visibility = View.GONE
                    }
                }
        )

        viewModel.chooseCity.observe(
                viewLifecycleOwner,
                EventObserver {
                    searchView.setQuery("", true)
                    viewModel.addCityToDB(it)
                }
        )

        viewModel.citySaved.observe(
                viewLifecycleOwner,
                {
                    goBack()
                }
        )

        viewModel.showError.observe(
                viewLifecycleOwner,
                EventObserver {
                    if (it == 404) {
                        showErrorMessage(getString(R.string.error_adding_city))
                    } else {
                        showErrorMessage(getString(R.string.error_text))
                    }
                }
        )

        viewModel.progressVisibility.observe(viewLifecycleOwner, { visibility ->
            changeProgressVisibility(visibility)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_options, menu)
        val searchItem = menu.findItem(R.id.actionSearch)
        searchView = searchItem.actionView as SearchView
        searchView.queryHint = getString(R.string.enter_city_name)
        searchView.setOnQueryTextListener(
                object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        search(query)
                        searchView.clearFocus()
                        return true
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        search(newText)
                        return true
                    }
                }
        )
        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                return goBack()
            }
        })
        searchView.setOnCloseListener {
            adapter.clearResults()
            return@setOnCloseListener true
        }
        searchItem.expandActionView()
        searchView.requestFocus()
        return super.onCreateOptionsMenu(menu, inflater)
    }

    private fun showErrorMessage(message: String) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_SHORT).show()
    }

    private fun search(query: String?) {
        if (query?.isNotEmpty() == true) {
            viewModel.search(query.toString())
        } else {
            adapter.clearResults()
        }
    }

    private fun goBack(): Boolean {
        return findNavController().popBackStack()
    }
}
