package ru.madbrains.perekrestok.weather.presentation.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.madbrains.perekrestok.weather.R
import ru.madbrains.perekrestok.weather.domain.model.CitySearchResult

class SearchResultAdapter(
    private val listener: (CitySearchResult) -> Unit
) : ListAdapter<CitySearchResult, SearchResultViewHolder>(SearchResultDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SearchResultViewHolder(
            inflater.inflate(R.layout.search_item, parent, false)
        ) { position ->
            listener.invoke(getItem(position)!!)
        }
    }

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        val message = getItem(position)
        message?.let {
            holder.bind(it)
        }
    }

    fun clearResults() {
        submitList(emptyList())
    }
}

class SearchResultViewHolder(
    view: View,
    private val listener: (Int) -> Unit
) : RecyclerView.ViewHolder(view) {

    private val name: TextView = itemView.findViewById(R.id.name)
    private val fullName: TextView = itemView.findViewById(R.id.fullName)

    init {
        itemView.setOnClickListener { listener.invoke(adapterPosition) }
    }

    fun bind(citySearchResult: CitySearchResult) {
        name.text = citySearchResult.name
        fullName.text = citySearchResult.fullName
    }
}

object SearchResultDiff : DiffUtil.ItemCallback<CitySearchResult>() {
    override fun areItemsTheSame(
        oldItem: CitySearchResult,
        newItem: CitySearchResult
    ): Boolean {
        return oldItem.fullName == newItem.fullName
    }

    override fun areContentsTheSame(oldItem: CitySearchResult, newItem: CitySearchResult): Boolean {
        return oldItem == newItem
    }
}