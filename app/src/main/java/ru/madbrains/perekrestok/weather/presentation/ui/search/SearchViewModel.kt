package ru.madbrains.perekrestok.weather.presentation.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import retrofit2.HttpException
import ru.madbrains.perekrestok.weather.domain.interactors.WeatherInteractor
import ru.madbrains.perekrestok.weather.domain.model.CitySearchResult
import ru.madbrains.perekrestok.weather.presentation.Event
import ru.madbrains.perekrestok.weather.presentation.ui.BaseViewModel
import timber.log.Timber

class SearchViewModel(
    private val interactor: WeatherInteractor
) : BaseViewModel() {

    val chooseCity = MutableLiveData<Event<CitySearchResult>>()
    val showError = MutableLiveData<Event<Int>>()
    val searchResults = MutableLiveData<List<CitySearchResult>>()
    val citySaved = MutableLiveData<Event<Unit>>()

    private val errorHandler = CoroutineExceptionHandler { _, exception ->
        progressVisibility.value = false
        var errorCode = 0
        if (exception is HttpException) {
            errorCode = exception.code()
        }
        showError.value = Event(errorCode)
        Timber.w(exception)
    }

    fun search(query: String) {
        viewModelScope.launch(errorHandler) {
            searchResults.value = interactor.searchCity(query)
        }
    }

    fun addCityToDB(city: CitySearchResult) {
        viewModelScope.launch(errorHandler) {
            progressVisibility.value = true
            interactor.addCityToDB(city.id)
            progressVisibility.value = false
            citySaved.value = Event(Unit)
        }
    }
}
